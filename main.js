// Define Global Variables
var listVar = '';
var listVarButton = '';

// Retrieve Select Menu Button ID
$(document).on('pageinit', function() {
$('[role="button"]').on('click', function() { 
    listVarButton = '#' + (this.id); 
    return listVarButton;
});
});

$(document).on('pageinit', function() {
$('[role="button"]').on('click', function() { 
    listVar = '#' + (this.id);
    toSlice = listVar.indexOf('-');
    listVar = listVar.slice(0,toSlice);
    return listVar;
});
});

// Set Default Select Menu Background
$(document).on('pageinit', function() {
$('[role="button"]').each(function() { 
    defaultValue = '#' + (this.id);
    toSlice = defaultValue.indexOf('-');
    defaultValue = defaultValue.slice(0,toSlice);
    defaultBackground = '#' + (this.id);
    $(defaultBackground).css('background-color', $(defaultValue).val());
});
});

// Selection Colour 
$(document).on('popupafteropen', function() {
	$(listVar).change(function() {
 		$(listVarButton).css('background-color', $(this).val());
    })
});

// Select Menu Background
$(document).on('popupcreate', function() {
        $('ul li a:contains(✔)').addClass('optionGreen');
        $('ul li a:contains(!)').addClass('optionOrange');
        $('ul li a:contains(✘)').addClass('optionRed');
});

// Text Field Data-State Colour Change 
$(document).ready(function() {
    $('input[type="text"]').on('change', function() {
        var field = $(this);
        var value = $.trim(field.val());

        $(field).parent().toggleClass('inputTextFill', value.length !==0);
    }).change();
});

//Prefetch
$('#landing').pagecontainer( "load", "index.html#cervical", { showLoadMsg: false } );
$('#landing').pagecontainer( "load", "index.html#thoracic", { showLoadMsg: false } );
$('#landing').pagecontainer( "load", "index.html#lumbar", { showLoadMsg: false } );
$('#landing').pagecontainer( "load", "index.html#respiration", { showLoadMsg: false } );
$('#landing').pagecontainer( "load", "index.html#stability", { showLoadMsg: false } );
$('#landing').pagecontainer( "load", "index.html#hips", { showLoadMsg: false } );
